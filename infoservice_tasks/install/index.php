<?php

global $DOCUMENT_ROOT;

IncludeModuleLangFile(__FILE__);

Class infoservice_tasks extends CModule
{
    var $MODULE_ID = "infoservice_tasks";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function infoservice_tasks()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = GetMessage('APPLICATION_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('APPLICATION_DESCRIPTION');
    }


    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        // Install events

        // регистрирууем обработчик для модуля
        RegisterModuleDependences("tasks","OnTaskUpdate","infoservice_tasks","cDeferredTasks","OnTaskUpdate");

        // установка модуля
        RegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile(GetMessage('APPLICATION_INSTALLING'), $DOCUMENT_ROOT."/bitrix/modules/infoservice_tasks/install/step.php");
        return true;
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;

        // удаляем обработчик для модуля
        UnRegisterModuleDependences("tasks","OnTaskUpdate","infoservice_tasks","cDeferredTasks","OnTaskUpdate");

        // удаление модуля
        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile(GetMessage('APPLICATION_UNINSTALLING'), $DOCUMENT_ROOT."/bitrix/modules/infoservice_tasks/install/unstep.php");
        return true;
    }

}
