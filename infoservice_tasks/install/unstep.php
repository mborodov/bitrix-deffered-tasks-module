<?php
    if(!check_bitrix_sessid()) return;

    // подключим lang файлы
    IncludeModuleLangFile(__FILE__);

    echo CAdminMessage::ShowNote(GetMessage('APPLICATION_IS_REMOVED'));
