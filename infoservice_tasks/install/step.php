<?php
    if(!check_bitrix_sessid()) return;

    // подключим lang файлы
    IncludeModuleLangFile(__FILE__);

    echo CAdminMessage::ShowNote(GetMessage('APPLICATION_IS_INSTALLED'));

?>

<h3><?php echo GetMessage('HOW_TO_USE')?>:</h3>

<ul>
    <li><?php echo GetMessage('FEATURE1')?>;</li>
    <li><?php echo GetMessage('FEATURE2')?>;</li>
</ul>
