<?php

// подключим lang файлы
IncludeModuleLangFile(__FILE__);

class cDeferredTasks {
    static $MODULE_ID="infoservice_tasks";

    const DEFFERED_STATUS = 6;
    /**
     * Хэндлер, отслеживающий изменения в задачах
     * @param $taskId
     * @return bool
     */
    static function OnTaskUpdate($taskId){
        $successDeffer = true;

        // используем данные текущего пользователя для создания инстансов
        global $USER;

        $userId = $USER->GetId();

        // получаем данные по текущей таске
        $task = CTaskItem::getInstance($taskId, $userId);
        $currentTaskData = $task->getData();
        $taskStatus = $currentTaskData['REAL_STATUS'];

        $message = '';
        if ($taskStatus == self::DEFFERED_STATUS) {
            $deadline = $currentTaskData['DEADLINE'];

            // проверим проставлена ли дата DEADLINE для текущей таски
            if (!empty($deadline)) {

                // дата дедлайна должна быть больше чем текущая
                if (strtotime($deadline) < time()) {
                    $successDeffer = false;
                    $message = GetMessage('OLD_DEADLINE');
                }
            } else {
                $successDeffer = false;
                $message = GetMessage('EMPTY_DEADLINE');
            }
        }

        if (!$successDeffer) {

            if(CModule::IncludeModule("im"))
            {
                CIMNotify::Add(array(
                    "TO_USER_ID" => $userId,
                    "FROM_USER_ID" => 0,
                    "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                    "NOTIFY_MODULE" => "infoservice_tasks",
                    "NOTIFY_MESSAGE" => $message
                ));
            }

            $task->startExecution();


        }

        return true;
    }
    
}