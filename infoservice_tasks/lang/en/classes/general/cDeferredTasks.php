<?php
    $MESS["EMPTY_DEADLINE"] = "To defer the task set the deadline date. #BR# The status of the task has been changed to \"In progress\".";
    $MESS["OLD_DEADLINE"] = "The deadline date must be after current date. #BR# The status of the task has been changed to \"In progress\".";
