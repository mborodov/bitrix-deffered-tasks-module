<?php
$MESS["APPLICATION_NAME"] = "Smart deferred tasks";
$MESS["APPLICATION_DESCRIPTION"] = "The application allows you automatically change deferred status of the task to overdue status at the deadline date.";
$MESS["APPLICATION_INSTALLING"] = "Installing module \"Smart deferred tasks\"";
$MESS["APPLICATION_UNINSTALLING"] = "Removing module \"Smart deferred tasks\"";