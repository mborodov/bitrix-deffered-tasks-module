<?php
$MESS["APPLICATION_IS_INSTALLED"] = "The module is installed and ready to go";
$MESS["HOW_TO_USE"] = "How to use";
$MESS["FEATURE1"] = "Set the deadline date of the task to which you want to defer it and then click \"Pause\" button. The task will be set in \"Deferred\" status.";
$MESS["FEATURE2"] = "After the deadline date the status of the task will be changed to \"Overdue\".";