Smart deffered tasks bitrix module
=============

The module allows you automatically change deferred status of the task to overdue status at the deadline date.

How to use:

* Set the deadline date of the task to which you want to defer it and then click "Pause" button. The task will be set in "Deferred" status.
* After the deadline date the status of the task will be changed to "Overdue".
